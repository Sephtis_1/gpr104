﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour
{

    public SpriteRenderer sr;
    public Transform tf;
    public float speed;
    private object playerPositon;
    private object transformPostion;
    private Vector3 playerPosition;
    public MonoBehaviour mb;


    // Use this for initialization
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        tf = GetComponent<Transform>();
        mb = GetComponent<MonoBehaviour>();
        playerPositon = transformPostion;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                //Move to the right
                tf.position = tf.position + Vector3.right;
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                //Move to the left
                tf.position = tf.position + Vector3.left;
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                //Move to downwards
                tf.position = tf.position + Vector3.down;
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                //Move to upwards
                tf.position = tf.position + Vector3.up;
            }
        }
        if (Input.GetKey(KeyCode.RightShift))
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                //Move to the right
                tf.position = tf.position + Vector3.right;
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                //Move to the left
                tf.position = tf.position + Vector3.left;
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                //Move to downwards
                tf.position = tf.position + Vector3.down;
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                //Move to upwards
                tf.position = tf.position + Vector3.up;
            }
        }
        if (Input.anyKey)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                //Move to the right
                tf.position = tf.position + Vector3.right;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                //Move to the left
                tf.position = tf.position + Vector3.left;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                //Move to downwards
                tf.position = tf.position + Vector3.down;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                //Move to upwards
                tf.position = tf.position + Vector3.up;
            }
            if (Input.GetKey(KeyCode.Space))
            {
                //reset position at center
                Vector3 startPos = default(Vector3);
                tf.position = startPos;
            }
            if (Input.GetKey(KeyCode.Q))
            {
                //Deactivated the object
                gameObject.SetActive(false);
            }
        }
        if (Input.GetKey(KeyCode.P))
        {
            if (GetComponent<MonoBehaviour>().enabled == true)
            {
                GetComponent<MonoBehaviour>().enabled = false;
            }
            if (GetComponent<MonoBehaviour>().enabled == false)
            {
                GetComponent<MonoBehaviour>().enabled = true;
            }
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}